import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { TypescriptComponent } from './components/typescript/typescript.component';
import { BasicComponent } from './components/basic/basic.component';
import { HomeComponent } from './components/home/home.component';
import { ReactiveFormsComponent } from './components/reactive-forms/reactive-forms.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LifecycleHooksComponent } from './components/lifecycle-hooks/lifecycle-hooks.component';

@NgModule({
  declarations: [
    AppComponent,
    BasicComponent,
    TypescriptComponent,
    HomeComponent,
    ReactiveFormsComponent,
    LifecycleHooksComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // import modułu dla Reactive Forms
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
