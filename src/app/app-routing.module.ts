import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BasicComponent } from './components/basic/basic.component';
import { TypescriptComponent } from './components/typescript/typescript.component';
import { HomeComponent } from './components/home/home.component';
import { ReactiveFormsComponent } from './components/reactive-forms/reactive-forms.component';
import { LifecycleHooksComponent } from './components/lifecycle-hooks/lifecycle-hooks.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'base', component: BasicComponent },
  { path: 'typescript', component: TypescriptComponent },
  { path: 'lifecycle-hooks', component: LifecycleHooksComponent },
  { path: 'reactive-forms', component: ReactiveFormsComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
